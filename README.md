Angular Starter Project
==

Projeto criado para facilitar a integração com outros frameworks de front end.

Frameworks integrados nesse projeto
--

 - Bootstrap
 - Font Awesome
 - ngx-toastr (https://github.com/scttcper/ngx-toastr)
